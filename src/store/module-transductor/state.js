export default {
  openMap: false,
  chartOptions: {
    phase_a: [],
    phase_b: [],
    phase_c: [],
    values: [],
    min: 0,
    max: 0,
    unit: '',
    dimension: '',
    status: false,
    graphType: ''
  },
  filterOptions: {
    dimension: 'Tensão',
    vision: '',
    startDate: null,
    endDate: null
  }
}
